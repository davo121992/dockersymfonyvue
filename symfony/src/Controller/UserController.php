<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("users")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="users")
     */
    public function users()
    {
        die;
        return $this->render('base.html.twig');
    }
}
